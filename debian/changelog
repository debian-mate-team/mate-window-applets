mate-window-applets (21.04.0-1) unstable; urgency=medium

  * New upstream release.
  * debian/control:
    + Bump Standards-Version: to 4.6.0. No changes needed.

 -- Mike Gabriel <sunweaver@debian.org>  Tue, 14 Dec 2021 08:07:28 +0100

mate-window-applets (20.04.0-2) unstable; urgency=medium

  * debian/watch:
    + Fix Github watch URL and switch to format version 4.

 -- Mike Gabriel <sunweaver@debian.org>  Thu, 29 Apr 2021 15:17:46 +0200

mate-window-applets (20.04.0-1) unstable; urgency=medium

  [ Mike Gabriel ]
  * debian/control:
    + Add Rules-Requires-Root: field and set it to no.
    + Bump Standards-Version: to 4.5.0. No changes needed.
  * debian/upstream/metadata:
    + Drop obsolete fields Contact: and Name:.
    + Append .git suffix to URLs in Repository: field.

  [ Martin Wimpress ]
  * New upstream release.

 -- Mike Gabriel <sunweaver@debian.org>  Sun, 16 Feb 2020 22:08:10 +0100

mate-window-applets (19.10.4-1) unstable; urgency=medium

  [ Martin Wimpress ]
  * New upstream release.

 -- Mike Gabriel <sunweaver@debian.org>  Sat, 28 Sep 2019 00:39:57 +0200

mate-window-applets (19.10.3-1) unstable; urgency=medium

  [ Martin Wimpress ]
  * New upstream release.
  * debian/mate-window-applets-common.install:
    + Update where fallback icons are installed.

  [ Mike Gabriel ]
  * debian/control:
    + Bump Standards-Version: to 4.4.0. No changes needed.
  * debian/upstream/metadata:
    + Add file. Comply with DEP-12.

 -- Mike Gabriel <sunweaver@debian.org>  Fri, 02 Aug 2019 17:23:39 +0200

mate-window-applets (1.5.1-3) unstable; urgency=medium

  * debian/control:
    + Update Vcs-*: fields. Package has been migrated to salsa.debian.org.
    + Bump Standards-Version: to 4.1.5. No changes needed.
    + Drop pkg-mate-team Alioth mailing list from Uploaders: field.
  * debian/copyright:
    + Make Upstream-Name: field's value more human readable.

 -- Mike Gabriel <sunweaver@debian.org>  Thu, 12 Jul 2018 13:47:30 +0200

mate-window-applets (1.5.1-2) unstable; urgency=medium

  [ Vangelis Mouhtsis ]
  * debian/control:
    + Bump Standards-Version: to 4.1.3. No changes needed.
    + Rename pretty name of our team -> Debian+Ubuntu MATE Packaging Team.
    + Update Maintainer: field to debian-mate ML on lists.debian.org.
    + Add myself to Uploaders: field.
    + Temporarily have pkg-mate-team ML under Uploaders:.

  [ Mike Gabriel ]
  * debian/copyright:
    + Use secure URI for copyright format.
  * debian/{control,copyright,watch}:
    + New upstream location updated.

 -- Mike Gabriel <sunweaver@debian.org>  Thu, 05 Apr 2018 22:45:15 +0200

mate-window-applets (1.5.1-1) unstable; urgency=medium

  * New upstream release.
  * debian/control:
    + Add D (mate-window-applets-common): libglib2.0-bin. (Closes: #881651).
  * debian/patches:
    + Drop 0001-fix-high-memory-consumption.patch. Included in this upstream
      release.

 -- Mike Gabriel <sunweaver@debian.org>  Tue, 14 Nov 2017 09:18:25 +0100

mate-window-applets (1.5-2) unstable; urgency=medium

  * debian/mate-window-applets-common.install:
    + Install icons into common bin:pkg.
  * debian/patches:
    + Add 0001-fix-high-memory-consumption.patch. Fix high memory consumption.
      Cherry-picked from upstream.

 -- Mike Gabriel <sunweaver@debian.org>  Mon, 30 Oct 2017 11:37:13 +0100

mate-window-applets (1.5-1) unstable; urgency=medium

  * Initial release to Debian. (Closes: #878179).

 -- Mike Gabriel <sunweaver@debian.org>  Tue, 10 Oct 2017 19:58:22 +0000
